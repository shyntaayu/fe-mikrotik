<?php if(!defined('BASEPATH')) exit('Zzzzzzz');

class User extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if( !$this->ion_auth->logged_in() || !$this->ion_auth->is_admin() )
		{
			redirect('login', 'refresh');
		}
	}

	public function index()
	{
		add_css('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');
		add_js(array(
			'plugins/jquery-datatable/jquery.dataTables.js',
			'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js',
			'plugins/jquery-datatable/jquery-datatable.js'
		));

		$data['users'] = $this->ion_auth->users()->result();
		$data['page'] = 'user';
		$this->load->view('view_engine/blank', $data);
	}
}