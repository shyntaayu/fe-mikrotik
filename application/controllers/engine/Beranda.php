<?php if(!defined('BASEPATH')) exit('Zzzzzzz');

class Beranda extends CI_Controller
{
	public function index()
	{
		if ( !$this->ion_auth->logged_in())
		{
			redirect('login', 'refresh');
		}
		$data['page'] = 'awal';
		$this->load->view('view_engine/blank', $data);
	}
}