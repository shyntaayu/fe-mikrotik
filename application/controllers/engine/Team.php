<?php if(!defined('BASEPATH')) exit('Zzzzzzz');

class Team extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// if( !$this->ion_auth->logged_in() || !$this->ion_auth->is_admin() )
		// {
		// 	redirect('login', 'refresh');
		// }

		$this->load->model('TimModel');
		$this->load->helper('string');

		$this->message = "";
		$this->users = $this->ion_auth->user()->row();
		$this->load->helper('download');
	}

	public function index()
	{
		if( !$this->ion_auth->logged_in() )
		{
			redirect('login', 'refresh');
		}

		add_css('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');
		add_js(array(
			'plugins/jquery-datatable/jquery.dataTables.js',
			'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js',
			'plugins/jquery-datatable/jquery-datatable.js'
		));

		// $users = $this->ion_auth->user()->row();

		if( $this->ion_auth->is_admin() )
		{
			$data['tim_kueri'] = $this->TimModel->listTimAdmin();
		}else{
			$data['tim_kueri'] = $this->TimModel->listTim($this->users->id);
		}

		$data['cekTim'] = $this->TimModel->cekTim( $this->users->id );
		// $data['tim_kueri'] = $this->TimModel->get_team();
		// print_r($data['tim_kueri']->result());die();
		$data['page'] = 'team';
		$this->load->view('view_engine/blank', $data);
	}

	public function tambah_team()
	{
		if( !$this->ion_auth->logged_in() )
		{
			redirect('login', 'refresh');
		}

		add_css(array(
			'plugins/sweetalert/sweetalert.css'
		));
		add_js(array(
			'plugins/jquery-validation/jquery.validate.js',
			'plugins/jquery-steps/jquery.steps.js',
			'plugins/sweetalert/sweetalert.min.js',
			'js/pages/forms/form-wizard.js',
			'js/bootstrap-filestyle.min.js'));

		// sekolah
		$nama_sekolah = $this->input->post('namaSekolah');
		$alamat_sekolah = $this->input->post('alamatSekolah');
		$telepon_sekolah = $this->input->post('telpSekolah');
		// tim
		$nama_tim = $this->input->post('namaTim');
		$nama_peserta1 = $this->input->post('namaPesertaSatu');
		$alamat_peserta1 = $this->input->post('alamatPesertaSatu');
		$telepon_peserta1 = $this->input->post('teleponPesertaSatu');
		$nama_peserta2 = $this->input->post('namaPesertaDua');
		$alamat_peserta2 = $this->input->post('alamatPesertaDua');
		$telepon_peserta2 = $this->input->post('teleponPesertaDua');
		// pendamping
		$nama_pendamping = $this->input->post('namaPendamping');
		$alamat_pendamping = $this->input->post('alamatPendamping');
		$telepon_pendamping = $this->input->post('teleponPendamping');
		$email_pendamping = $this->input->post('emailPendamping');

		### Uploader Config ###
		// $file_name = $idTim.'_'.$_FILES['userfile']['name'];
		$config['upload_path']='./upload/';
        $config['allowed_types']='gif|jpg|png|zip|rar';
        // $config['file_name'] = $file_name;
        $config['encrypt_name'] = TRUE;
        $config['max_size']=20480;

        $this->load->library('upload', $config);


		$this->form_validation->set_rules('namaSekolah', 'Nama Sekolah', 'trim|required');
		$this->form_validation->set_rules('alamatSekolah', 'Alamat Sekolah', 'trim|required');
		$this->form_validation->set_rules('telpSekolah', 'Telepon Sekolah', 'trim|required');
		$this->form_validation->set_rules('namaTim', 'Nama Tim', 'trim|required');
		$this->form_validation->set_rules('namaPesertaSatu', 'Nama Peserta 1', 'trim|required');
		$this->form_validation->set_rules('alamatPesertaSatu', 'Alamat Peserta 1', 'trim|required');
		$this->form_validation->set_rules('teleponPesertaSatu', 'Nomor Telepon Peserta 1', 'trim|required');
		$this->form_validation->set_rules('namaPesertaDua', 'Nama Peserta 2', 'trim|required');
		$this->form_validation->set_rules('alamatPesertaDua', 'Alamat Peserta 2', 'trim|required');
		$this->form_validation->set_rules('teleponPesertaDua', 'Telepon Peserta 2', 'trim|required');
		$this->form_validation->set_rules('namaPendamping', 'Nama Pendamping', 'trim|required');
		$this->form_validation->set_rules('alamatPendamping', 'Alamat Pendamping', 'trim|required');
		$this->form_validation->set_rules('teleponPendamping', 'Telepon Pendamping', 'trim|required');
		$this->form_validation->set_rules('emailPendamping', 'Email Pendamping', 'trim|required');
		$this->form_validation->set_message('required', '%s harus diisi.');

		if( $this->form_validation->run() === FALSE)
		{
			$errors = validation_errors();
		}else
		{
			### START Upload Region ###

			if( !$this->upload->do_upload('userfile') )
			{
				$error = $this->upload->display_errors('<p>', '</p>');
				$this->session->set_flashdata('errors', $error);
				// print_r($error); die();
			}else{
				$data = $this->upload->data();
				$this->session->set_flashdata('message', "Upload Data Sukses");
				$filename = $data['file_name'];
				// echo '<pre>';
				// print_r($data); die();
				// echo '</pre>';
			}


			### END Upload Region ###

			$data = array(
					"verifikasi" => strtoupper(random_string('alnum', 8)),
					"nama_tim" => $nama_tim,
					"nama_sekolah" => $nama_sekolah,
					"alamat_sekolah" => $alamat_sekolah,
					"telepon" => $telepon_sekolah,
					"nama_pendamping" => $nama_pendamping,
					"alamat_pendamping" => $alamat_pendamping,
					"email_pendamping" => $email_pendamping,
					"telepon_pendamping" => $telepon_pendamping,
					"nama_peserta1" => $nama_peserta1,
					"alamat_peserta1" => $alamat_peserta1,
					"telepon_peserta1" => $telepon_peserta1,
					"nama_peserta2" => $nama_peserta2,
					"alamat_peserta2" => $alamat_peserta2,
					"telpon_peserta2" => $telepon_peserta2,
					"id_pendaftar" => $this->ion_auth->user()->row()->id,
					"status" => 0,
					"data_upload" => !empty($filename) ? $filename : ""
			);
			// print_r($data);die();
			if ( $this->TimModel->saveTim($data) )
			{
				$this->session->set_flashdata('message', 'Berhasil menyimpan data tim '.$nama_tim );
				redirect('panel/data/tim/', 'refresh');
			}else{
				$this->session->set_flashdata('errors', 'Gagal menyimpan data tim '.$nama_tim );
				redirect('panel/data/tim/tambah', 'refresh');
			}
		}


		$data['page'] = 'timWizard';
		$this->load->view('view_engine/blank', $data);
	}

	/** fungsi upload data
	 * dimatikan
	 */
	public function upload_berkas($idTim)
	{
		// $file_name = $idTim.'_'.$_FILES['userfile']['name'];
		$config['upload_path']='./upload/';
        $config['allowed_types']='gif|jpg|png|zip|rar';
        // $config['file_name'] = $file_name;
        $config['overwrite'] = TRUE;
        $config['max_size']=20480;

        $path = $config['upload_path'].$idTim;

        $this->load->library('upload', $config);

        if( $this->input->post() )
        {
        	if ( ! $this->upload->do_upload('userfile'))
	        {
	                $error = array('error' => $this->upload->display_errors());

	                print_r($error);
	        }
	        else
	        {
	                // $data['data_upload'] = $this->upload->data();

	        		// tinggal menyimpan nama file di dalam database
	                $token = $this->input->post('token_foto');
	                $nama=$this->upload->data('file_name');

	                $data = array(
	                	'data_upload' => $idTim.$token.$nama
	                );

	                print_r($data);

	                /*if ( $this->TimModel->ubahTim($idTim, $data) )
					{
						$this->session->set_flashdata('message', 'Berhasil merubah data tim '.$nama_tim );
						redirect('panel/data/tim/', 'refresh');
					}else{
						$this->session->set_flashdata('errors', 'Gagal merubah data tim '.$nama_tim );
						redirect(uri_string(), 'refresh');
					}*/
	        }
        }
	}

	public function upload($idTim)
	{
		$data['idTim'] = $idTim;
		$data['page'] = 'upload';
		$this->load->view('view_engine/blank', $data);
	}

	/**
	* Fungsi untuk edit team
	*
	* @return boolean
	*/
	public function edit_team($id_tim)
	{

		if( !$this->ion_auth->logged_in() )
		{
			redirect('login', 'refresh');
		}


		if( !$this->TimModel->ambilData($this->users->id) )
		{
			show_404();
		}
		
		/*if($this->input->post())
		{
			echo "<pre>";
			print_r( $this->input->post() );die();
			echo "</pre>";
		}*/

		// $user = $this->ion_auth->user()->row();

		// sekolah
		$nama_sekolah = $this->input->post('namaSekolah');
		$alamat_sekolah = $this->input->post('alamatSekolah');
		$telepon_sekolah = $this->input->post('telpSekolah');
		// tim
		$nama_tim = $this->input->post('namaTim');
		$nama_peserta1 = $this->input->post('namaPesertaSatu');
		$alamat_peserta1 = $this->input->post('alamatPesertaSatu');
		$telepon_peserta1 = $this->input->post('teleponPesertaSatu');
		$nama_peserta2 = $this->input->post('namaPesertaDua');
		$alamat_peserta2 = $this->input->post('alamatPesertaDua');
		$telepon_peserta2 = $this->input->post('teleponPesertaDua');
		// pendamping
		$nama_pendamping = $this->input->post('namaPendamping');
		$alamat_pendamping = $this->input->post('alamatPendamping');
		$telepon_pendamping = $this->input->post('teleponPendamping');
		$email_pendamping = $this->input->post('emailPendamping');

		$status = $this->input->post('status');

		### Uploader Config ###
		// $file_name = $idTim.'_'.$_FILES['userfile']['name'];
		$config['upload_path']='./upload/';
        $config['allowed_types']='gif|jpg|png|zip|rar';
        // $config['file_name'] = $file_name;
        $config['encrypt_name'] = TRUE;
        $config['max_size']=20480;

        $this->load->library('upload', $config);


		$this->form_validation->set_rules('namaSekolah', 'Nama Sekolah', 'trim|required');
		$this->form_validation->set_rules('alamatSekolah', 'Alamat Sekolah', 'trim|required');
		$this->form_validation->set_rules('telpSekolah', 'Telepon Sekolah', 'trim|required');
		$this->form_validation->set_rules('namaTim', 'Nama Tim', 'trim|required');
		$this->form_validation->set_rules('namaPesertaSatu', 'Nama Peserta 1', 'trim|required');
		$this->form_validation->set_rules('alamatPesertaSatu', 'Alamat Peserta 1', 'trim|required');
		$this->form_validation->set_rules('teleponPesertaSatu', 'Nomor Telepon Peserta 1', 'trim|required');
		$this->form_validation->set_rules('namaPesertaDua', 'Nama Peserta 2', 'trim|required');
		$this->form_validation->set_rules('alamatPesertaDua', 'Alamat Peserta 2', 'trim|required');
		$this->form_validation->set_rules('teleponPesertaDua', 'Telepon Peserta 2', 'trim|required');
		$this->form_validation->set_rules('namaPendamping', 'Nama Pendamping', 'trim|required');
		$this->form_validation->set_rules('alamatPendamping', 'Alamat Pendamping', 'trim|required');
		$this->form_validation->set_rules('teleponPendamping', 'Telepon Pendamping', 'trim|required');
		$this->form_validation->set_rules('emailPendamping', 'Email Pendamping', 'trim|required');
		$this->form_validation->set_message('required', '%s harus diisi.');


		if( $this->form_validation->run() === FALSE)
		{
			$errors = validation_errors();
		}else
		{
			### START Upload Region ###

			if ( $_FILES AND $_FILES['userfile']['name'] ) 
			{
			    if( !$this->upload->do_upload('userfile') )
				{
					$error = $this->upload->display_errors('<p>', '</p>');
					$this->session->set_flashdata('errors', 'Gagal Upload data');
					// print_r($error); die();
				}else{
					$data = $this->upload->data();

					$filename = $data['file_name'];
					/*echo '<pre>';
					print_r($data); die();
					echo '</pre>';*/
				}
			}


			### END Upload Region ###

			$data = array(
					// "verifikasi" => strtoupper(random_string('alnum', 8)),
					"nama_tim" => $nama_tim,
					"nama_sekolah" => $nama_sekolah,
					"alamat_sekolah" => $alamat_sekolah,
					"telepon" => $telepon_sekolah,
					"nama_pendamping" => $nama_pendamping,
					"alamat_pendamping" => $alamat_pendamping,
					"email_pendamping" => $email_pendamping,
					"telepon_pendamping" => $telepon_pendamping,
					"nama_peserta1" => $nama_peserta1,
					"alamat_peserta1" => $alamat_peserta1,
					"telepon_peserta1" => $telepon_peserta1,
					"nama_peserta2" => $nama_peserta2,
					"alamat_peserta2" => $alamat_peserta2,
					"telpon_peserta2" => $telepon_peserta2,
					// "id_pendaftar" => $this->ion_auth->user()->row()->id,
					// "status" => 0,
					"data_upload" => isset($filename) ? $filename : ""
			);

			if( $this->ion_auth->is_admin() )
			{
				$data['status'] = $status;
			}

			// edit disini
			if ( $this->TimModel->ubahTim($id_tim, $data) )
			{
				$this->session->set_flashdata('message', 'Berhasil merubah data tim '.$nama_tim );
				redirect('panel/data/tim/', 'refresh');
			}else{
				$this->session->set_flashdata('errors', 'Gagal merubah data tim '.$nama_tim );
				redirect(uri_string(), 'refresh');
			}
		}


		if ( $this->TimModel->get_team($id_tim, $this->users->id)->num_rows() < 1 ){
			show_error('Halaman tidak ditemukan');
			redirect('panel', 'refresh');
		}

		$data['tim'] = $this->TimModel->get_team($id_tim, $this->users->id);

		$data['page'] = 'editTeam';
		$this->load->view('view_engine/blank', $data);
	}

	public function delete_team($id)
	{
		if( !$this->ion_auth->logged_in() )
		{
			redirect('login', 'refresh');
		}

		if( $this->ion_auth->is_admin() ) // jika admin
		{
			if( $this->TimModel->hapus_tim($id, TRUE) )
			{
				$this->session->set_flashdata('message', 'Admin: Tim berhasil dihapus');
				redirect('panel/data/tim', 'redirect');
			}else{
				$this->session->set_flashdata('message', 'Admin: Tim gagal dihapus');
				redirect('panel/data/tim', 'redirect');
			}
		}else{ // jika selain admin
			if( $this->TimModel->hapus_tim($id) )
			{
				$this->session->set_flashdata('message', 'User: Tim berhasil dihapus');
				redirect('panel/data/tim', 'redirect');
			}else{
				$this->session->set_flashdata('message', 'USer: Tim gagal dihapus');
				redirect('panel/data/tim', 'redirect');
			}
		}

		
	}

	public function download_berkas($lokasi)
	{
		if( !$this->ion_auth->logged_in() )
		{
			redirect('login', 'refresh');
		}

		$path = './upload/'.$lokasi;
		return force_download($path, NULL);
	}


	// /**
	//  *
	//  * Gak di pake
	//  *
	//  * @author Angga Lanuma :v
	//  *
	//  **/
	// public function tambah_tim()
	// {
	// 	// sekolah
	// 	$nama_sekolah = $this->input->post('namaSekolah');
	// 	$alamat_sekolah = $this->input->post('alamatSekolah');
	// 	$telepon_sekolah = $this->input->post('telpSekolah');
	// 	// tim
	// 	$nama_tim = $this->input->post('namaTim');
	// 	$nama_peserta1 = $this->input->post('namaPesertaSatu');
	// 	$alamat_peserta1 = $this->input->post('alamatPesertaSatu');
	// 	$telepon_peserta1 = $this->input->post('teleponPesertaSatu');
	// 	$nama_peserta2 = $this->input->post('namaPesertaDua');
	// 	$alamat_peserta2 = $this->input->post('alamatPesertaDua');
	// 	$telepon_peserta2 = $this->input->post('teleponPesertaDua');
	// 	// pendamping
	// 	$nama_pendamping = $this->input->post('namaPendamping');
	// 	$alamat_pendamping = $this->input->post('alamatPendamping');
	// 	$telepon_pendamping = $this->input->post('teleponPendamping');
	// 	$email_pendamping = $this->input->post('emailPendamping');

	// 	// upload method
	// 	$config['upload_path'] = './uploads/';
	// 	$config['allowed_types'] = 'jpg|png|zip|rar|tar|tar.gz';
	// 	$config['max_size']     = '51200';
	// 	$config['encrypt_name'] = 'TRUE';

	// 	$this->load->library('upload', $config);

	// 	$this->form_validation->set_rules('namaSekolah', 'Nama Sekolah', 'trim|required');
	// 	$this->form_validation->set_rules('alamatSekolah', 'Alamat Sekolah', 'trim|required');
	// 	$this->form_validation->set_rules('telpSekolah', 'Telepon Sekolah', 'trim|required');
	// 	$this->form_validation->set_rules('namaTim', 'Nama Tim', 'trim|required');
	// 	$this->form_validation->set_rules('namaPesertaSatu', 'Nama Peserta 1', 'trim|required');
	// 	$this->form_validation->set_rules('alamatPesertaSatu', 'Alamat Peserta 1', 'trim|required');
	// 	$this->form_validation->set_rules('teleponPesertaSatu', 'Nomor Telepon Peserta 1', 'trim|required');
	// 	$this->form_validation->set_rules('namaPesertaDua', 'Nama Peserta 2', 'trim|required');
	// 	$this->form_validation->set_rules('alamatPesertaDua', 'Alamat Peserta 2', 'trim|required');
	// 	$this->form_validation->set_rules('teleponPesertaDua', 'Telepon Peserta 2', 'trim|required');
	// 	$this->form_validation->set_rules('namaPendamping', 'Nama Pendamping', 'trim|required');
	// 	$this->form_validation->set_rules('alamatPendamping', 'Alamat Pendamping', 'trim|required');
	// 	$this->form_validation->set_rules('teleponPendamping', 'Telepon Pendamping', 'trim|required');
	// 	$this->form_validation->set_rules('emailPendamping', 'Email Pendamping', 'trim|required');

	// 	if( $this->form_validation->run() === FALSE)
	// 	{
	// 		print_r(validation_errors());
	// 	}else
	// 	{

	// 		$data = 'atas';
	// 		if ( $this->upload->do_upload())
 //            {
 //                $filename = array('upload_data' => $this->upload->data());

 //                $data = $filename;
 //                // $data = 'tengah';
 //            }
 //            else
 //            {
 //            	$error = array('error' => $this->upload->display_errors('<p>', '</p>'));

 //                // $this->message = $error;
 //                $data = 'bawah';

                
 //            }

	// 		$data = array(
	// 				"verifikasi" => strtoupper(random_string('alnum', 8)),
	// 				"nama_tim" => $nama_tim,
	// 				"nama_sekolah" => $nama_sekolah,
	// 				"alamat_sekolah" => $alamat_sekolah,
	// 				"telepon" => $telepon_sekolah,
	// 				"nama_pendamping" => $nama_pendamping,
	// 				"alamat_pendamping" => $alamat_pendamping,
	// 				"email_pendamping" => $email_pendamping,
	// 				"telepon_pendamping" => $telepon_pendamping,
	// 				"nama_peserta1" => $nama_peserta1,
	// 				"alamat_peserta1" => $alamat_peserta1,
	// 				"telepon_peserta1" => $telepon_peserta1,
	// 				"nama_peserta2" => $nama_peserta2,
	// 				"alamat_peserta2" => $alamat_peserta2,
	// 				"telpon_peserta2" => $telepon_peserta2,
	// 				"data_upload" => isset($filename) ? $filename : ""
	// 		);

			
	// 		print_r($data);

	// 	}

	// }

	/**
	 * Fungsi inputan
	 *
	 * @return String
	 */
	private function inputan( $name )
	{
		return $this->input->post($name);
	}
}