<?php if( !defined('BASEPATH') ) exit('Zzzzzz');

class User_model extends CI_Model
{
	function get_user()
	{
		$query = $this->select('username, email, active, first_name, last_name, company, phone');
		return $query->get(users);
	}
}