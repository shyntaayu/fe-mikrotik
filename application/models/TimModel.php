<?php 

class TimModel extends CI_Model
{

	/*gak kepake*/
	public function get_team( $idTim, $id_user, $is_admin=FALSE)
	{
		$kueri = $this->db->select('*');
		
		if( !isset($idTim) && !isset($id_user) )
		{
			show_404();
		}elseif( !empty($idTim) && $is_admin === FALSE ){ // user biasa
			$kueri = $this->db->where('id_pendaftar', $id_user);
			$kueri = $this->db->join('users', 'users.id = data_tim.id_pendaftar');
		}elseif( !empty($idTim) && $is_admin === TRUE){ // maka admin

		}

		$kueri = $this->db->get('data_tim');

		return $kueri;
	}

	/**
	 * List Tim buat user
	 *
	 * @return object
	 */
	public function listTim($id_user)
	{
		$kueri = $this->db->select('data_tim.id,data_tim.nama_sekolah, nama_tim, data_tim.id_pendaftar, users.first_name, data_tim.status');
		$kueri = $this->db->where('id_pendaftar', $id_user);
		$kueri = $this->db->join('users', 'users.id = data_tim.id_pendaftar');
		$kueri = $this->db->get('data_tim');

		return $kueri;
	}

	/**
	 * List Tim buat admin
	 *
	 * @return object
	 */
	public function listTimAdmin()
	{
		$kueri = $this->db->select('data_tim.id,data_tim.nama_sekolah, nama_tim, data_tim.id_pendaftar, users.first_name, data_tim.status');
		// $kueri = $this->db->where('id_pendaftar', $id_user);
		$kueri = $this->db->join('users', 'users.id = data_tim.id_pendaftar');
		$kueri = $this->db->get('data_tim');

		return $kueri;
	}

	public function saveTim($data)
	{
		return $this->db->insert('data_tim', $data);
	}

	public function ubahTim($idTim, $data)
	{
		$this->db->where('id', $idTim);
		$this->db->update('data_tim', $data);
		if($this->db->affected_rows())
		{
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function dataUpload($idTim, $data)
	{
		$this->db->where('id', $idTim);
		$this->db->update('data_tim', $data);
		if($this->db->affected_rows())
		{
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/**
	 * Cek tim, apakah sudah ada tim terdaftar apa belum
	 *
	 * @return boolean
	 **/
	public function cekTim($id_user)
	{
		$kueri = $this->db->select('*')
						->where('id_pendaftar', $id_user)
						->get('data_tim');

		if($kueri->num_rows() === 1 )
		{
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/**
	 * fungsi buat ambil single row tim
	 *
	 *
	 */
	function ambilData($id_user)
	{
		$kueri = $this->db->where('id_pendaftar', $id_user);
		$kueri = $this->db->get('data_tim');
		$row = $kueri->row();

		if( isset($row) )
		{
			return $row->id;
		}
	}

	/**
	 * Menghapus tim
	 *
	 * @return TRUE jika tim berhasil dihapus
	 */
	function hapus_tim($id, $is_admin=FALSE)
	{
		$this->db->where('id',$id);
		if( $is_admin === FALSE )
		{
			$this->db->where('id_pendaftar', $this->ion_auth->user()->row()->id);
		}
		$this->db->delete('data_tim');
		if($this->db->affected_rows())
		{
			return TRUE;
		}else{
			return FALSE;
		}
	}
}