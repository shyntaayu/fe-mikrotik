<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>
            Lomba Mikrotik STIMATA
        </title>
        <meta content="" name="description">
        <meta content="" name="author">
        <meta content="" name="keywords">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <!-- Sky v1.3 || ex nihilo || November 2014 - April 2015 -->
        <!-- style start -->
        <link href="<?php echo assets_url();?>cssutama/style-dark.css" media="all" rel="stylesheet" type="text/css">
        <!-- <link href="styleswitch/styleswitch.css" media="all" rel="stylesheet" type="text/css"> -->
        <!-- alternate style start || to use your preferred color, simply remove all style colors below and leave only your preferred color -->
        <!-- <link href="css/colors/red-dark.css" media="screen" rel="stylesheet" title="red" type="text/css"> -->
        <link href="<?php echo assets_url(); ?>cssutama/colors/red-2-dark.css" media="screen" rel="stylesheet" title="red-2" type="text/css">
        <!-- <link href="css/colors/green-dark.css" media="screen" rel="stylesheet" title="green" type="text/css">
        <link href="css/colors/green-2-dark.css" media="screen" rel="stylesheet" title="green-2" type="text/css">
        <link href="css/colors/blue-dark.css" media="screen" rel="stylesheet" title="blue" type="text/css">
        <link href="css/colors/blue-2-dark.css" media="screen" rel="stylesheet" title="blue-2" type="text/css">
        <link href="css/colors/yellow-dark.css" media="screen" rel="stylesheet" title="yellow" type="text/css">
        <link href="css/colors/yellow-2-dark.css" media="screen" rel="stylesheet" title="yellow-2" type="text/css">
        <link href="css/colors/orange-dark.css" media="screen" rel="stylesheet" title="orange" type="text/css">
        <link href="css/colors/gold-dark.css" media="screen" rel="stylesheet" title="gold" type="text/css">
        <link href="css/colors/pink-dark.css" media="screen" rel="stylesheet" title="pink" type="text/css">
        <link href="css/colors/purple-dark.css" media="screen" rel="stylesheet" title="purple" type="text/css">
        <link href="css/colors/violet-dark.css" media="screen" rel="stylesheet" title="violet" type="text/css">
        <link href="css/colors/turquoise-dark.css" media="screen" rel="stylesheet" title="turquoise" type="text/css">
        <link href="css/colors/brick-dark.css" media="screen" rel="stylesheet" title="brick" type="text/css">
        <link href="css/colors/silver-dark.css" media="screen" rel="stylesheet" title="silver" type="text/css"> --><!-- alternate style end -->
        <link href="<?php echo assets_url(); ?>cssutama/skeleton-wide.css" media="all" rel="stylesheet" type="text/css">
        <link href="<?php echo assets_url(); ?>cssutama/media.css" media="all" rel="stylesheet" type="text/css">
        <link href="<?php echo assets_url(); ?>cssutama/font-awesome-4.3.0/css/font-awesome.css" media="all" rel="stylesheet" type="text/css"><!-- style end -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- screen loader start -->
        <div class="screen-loader"></div><!-- screen loader end -->
        <!-- preload start -->
        <div id="preload">
            <!-- preload status start -->
            <div id="preload-status"></div><!-- preload status end -->
        </div><!-- preload end -->
        <!-- kenburnsy bg start -->
        <div id="kenburnsy-bg">
            <img alt="Background 1" src="<?php echo assets_url();?>images/background/1.jpg">
            <img alt="Background 2" src="<?php echo assets_url();?>images/background/2.jpg">
            <img alt="Background 3" src="<?php echo assets_url();?>images/background/3.jpg">
            <img alt="Background 4" src="<?php echo assets_url();?>images/background/4.jpg">
            <img alt="Background 5" src="<?php echo assets_url();?>images/background/5.jpg">
        </div><!-- kenburnsy bg end -->
        <!-- particles start -->
        <div id="particles-js"></div><!-- particles end -->
        <!-- curtains start -->
        <div id="curtains"></div><!-- curtains end -->
        <!-- top shade start -->
        <div id="top-shade"></div><!-- top shade end -->
        <!-- preload content start -->
        <div class="preload-content">
        </div><!-- preload content end -->
        <!-- menu start -->
        <nav class="menu">
            <ul class="mainNav">
                <li class="curr">
                    <a class="menu-state active" href="#" id="fire-home"><span class="navNumber">00</span> <span class="navTitle">Beranda</span></a>
                </li>
                <li>
                    <a class="menu-state" href="#" id="fire-about"><span class="navNumber">01</span> <span class="navTitle">Persyaratan</span></a>
                </li>
                <li>
                    <a class="menu-state" href="<?php echo base_url(); ?>daftar"><span class="navNumber">02</span> <span class="navTitle">Pendaftaran</span></a>
                </li>
                <li>
                	<a href="#" class="menu-state" id="fire-contact"><span class="navNumber">03</span><span class="navTitle">Kontak</span></a>
                </li>
            </ul>
        </nav><!-- menu end -->
        <!-- menu mobile start -->
        <nav class="menu-mobile">
            <ul>
                <li>
                    <ul>
                        <li class="lifting-first">
                            <a class="menu-state active" href="#" id="fire-home-mobile">Beranda</a>
                        </li>
                        <li class="lifting">
                            <a class="menu-state" href="#" id="fire-about-mobile">Persyaratan</a
                        </li>
                        <li class="lifting">
                            <a class="menu-state" href="<?php echo base_url(); ?>daftar" >Pendaftaran</a>
                        </li>
                        <li class="lifting">
                            <a class="menu-state" href="#" id="fire-contact-mobile">Kontak</a>
                        </li>
                    </ul>
                    <a class="menu-mobile-trigger" href="#"><span class="menu-icon"></span></a>
                </li>
            </ul>
        </nav><!-- menu mobile end -->
        <!-- upper page start -->
        <div class="upper-page current" id="home">
            <!-- center container start -->
            <div class="center-container-home">
                <!-- center block start -->
                <div class="center-block">
                    <!-- borders start -->
                    <div class="borders"></div><!-- borders end -->
                    <!-- social icons start -->
                    <div class="social-icons-wrapper">
                        <ul class="social-icons">
                            <li>
                                <a class="fa fa-instagram fa-2x" target="_BLANK" href="https://www.instagram.com/stimata_malang/"></a>
                            </li>
                            <li>
                                <a class="fa fa-facebook-square fa-2x" target="_BLANK" href="https://www.facebook.com/stimatamalang/"></a>
                            </li>
                            <li>
                                <a class="fa fa-youtube-square fa-2x" target="_BLANK" href="https://www.youtube.com/channel/UCuoVbwNyvKmrY_hizoSUclQ"></a>
                            </li>
                        </ul>
                    </div><!-- social icons end -->
                    <!-- upper content start -->
                    <div class="upper-content">
                        <!-- container start -->
                        <div class="container center">
                            <!-- intro wrapper start -->
                            <div id="intro-wrapper">
                                <div class="eight columns column">
                                    <div id="intro-top-line"></div>
                                </div>
                                <div class="eight columns column">
                                    <div id="intro-top">
                                        Lomba Mikrotik
                                    </div>
                                </div>
                                <div class="sixteen columns">
                                    <div id="intro-title">
                                        STIMATA
                                    </div>
                                </div>
                                <div class="eight columns column">
                                    <!-- teaser start -->
                                    <div id="teaser">
                                        &nbsp;
                                    </div><!-- teaser end -->
                                </div>
                                <div class="eight columns column">
                                    <div id="intro-bottom-line"></div>
                                </div>
                            </div><!-- intro wrapper end -->
                        </div><!-- container end -->
                    </div><!-- upper content end -->
                    <!-- launch countdown start -->
                    <div id="countdown-wrapper">
                        <div id="countdown-wrap">
                            <ul id="countdown">
                                <li>
                                    <span id="days-sub">days</span> <input class="knob" data-fgcolor="#fff" data-height="140" data-max="60" data-min="0" data-readonly="true" data-skin="tron"
                                    data-thickness="0.1" data-width="140" id="days">
                                </li>
                                <li>
                                    <span id="hours-sub">hours</span> <input class="knob" data-fgcolor="#fff" data-height="140" data-max="24" data-min="0" data-readonly="true" data-skin="tron"
                                    data-thickness="0.1" data-width="140" id="hours">
                                </li>
                                <li>
                                    <span id="mins-sub">minutes</span> <input class="knob" data-fgcolor="#fff" data-height="140" data-max="60" data-min="0" data-readonly="true" data-skin="tron"
                                    data-thickness="0.1" data-width="140" id="mins">
                                </li>
                                <li>
                                    <span id="secs-sub">seconds</span> <input class="knob" data-fgcolor="#fff" data-height="140" data-max="60" data-min="0" data-readonly="true" data-skin="tron"
                                    data-thickness="0.1" data-width="140" id="secs">
                                </li>
                            </ul>
                        </div>
                    </div><!-- launch countdown end -->
                    <!-- newsletter form start -->
                    <div id="subscribe-form">
                        <img src="https://bsbproduction.s3.amazonaws.com/portals/8476/images/becomeasponsor.png" width="60%">                        
                    </div><!-- newsletter form end -->
                </div><!-- center container end -->
            </div><!-- center block end -->
        </div><!-- upper page end -->
        <!-- about start -->
        <div class="lower-page" id="about">
            <!-- center container start -->
            <div class="center-container">
                <!-- center block start -->
                <div class="center-block">
                    <!-- lower content start -->
                    <div class="lower-content">
                        <!-- about intro start -->
                        <div class="about-intro">
                            <h1>
                                Persyaratan
                            </h1>
                        </div><!-- about intro end -->
                        <!-- container start -->
                        <div class="container ">
                            <!-- divider left top start -->
                            <div class="sixteen columns">
                                <div class="divider-left-top"></div>
                            </div><!-- divider left top end -->
                            <!-- divider right top start -->
                            <div class="sixteen columns">
                                <div class="divider-right-top"></div>
                            </div><!-- divider right top end -->
                            <!-- about full start -->
                            <div class="sixteen columns about-column-full">
                                <p>
                                    <img src="<?php echo assets_url();?>images/persyaratan.png" width="80%">
                                </p>
                            </div><!-- about full end -->
                            
                            <!-- divider left bottom start -->
                            <div class="sixteen columns">
                                <div class="divider-left-bottom"></div>
                            </div><!-- divider left bottom end -->
                            <!-- divider right bottom start -->
                            <div class="sixteen columns">
                                <div class="divider-right-bottom"></div>
                            </div><!-- divider right bottom end -->
                        </div><!-- container end -->
                        <!-- closer start -->
                        <div class="fire-closer">
                            <a class="menu-state" href="#" id="fire-about-closer"><img alt="Closer" src="<?php echo  assets_url(); ?>images/closer.png"></a>
                        </div><!-- closer end -->
                    </div><!-- lower content end -->
                </div><!-- center container end -->
            </div><!-- center block end -->
        </div><!-- about end -->
        <!-- services start -->
        <div class="lower-page" id="services">
            <!-- center container start -->
            <div class="center-container">
                <!-- center block start -->
                <div class="center-block">
                    <!-- lower content start -->
                    <div class="lower-content">
                        <!-- services intro start -->
                        <div class="services-intro">
                            <h1>
                                SERVICES
                            </h1>
                        </div><!-- services intro end -->
                        <!-- container start -->
                        <div class="container center">
                            <!-- divider left top start -->
                            <div class="sixteen columns">
                                <div class="divider-left-top"></div>
                            </div><!-- divider left top end -->
                            <!-- divider right top start -->
                            <div class="sixteen columns">
                                <div class="divider-right-top"></div>
                            </div><!-- divider right top end -->
                            <!-- services full start -->
                            <div class="sixteen columns services-column-full">
                                <p>
                                    Lorem ipsum dolor sit amet, <a href="index-ken-burns-dark-4.html#">consectetur adipiscing elit</a>. Donec placerat congue leo, nec vehicula elit vehicula vel. Duis odio nibh, malesuada sed
                                    luctus ut, consectetur vel dui. Mauris vel imperdiet nulla. Sed eu ligula augue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
                                    himenaeos. Sed a justo nec elit feugiat facilisis quis quis elit.
                                </p>
                            </div><!-- services full end -->
                            <!-- services column 1 start -->
                            <div class="one-third column services-column">
                                <span class="awesome"><span class="fa fa-cogs"></span></span>
                                <p class="info">
                                    Easy Customization
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, <a href="index-ken-burns-dark-4.html#">consectetur adipiscing elit</a>. Sed risus purus, pellentesque a consequat a, accumsan vel libero.
                                </p>
                            </div><!-- services column 1 end -->
                            <!-- services column 2 start -->
                            <div class="one-third column services-column">
                                <span class="awesome"><span class="fa fa-users"></span></span>
                                <p class="info">
                                    User Friendly
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, <a href="index-ken-burns-dark-4.html#">consectetur adipiscing elit</a>. Sed risus purus, pellentesque a consequat a, accumsan vel libero.
                                </p>
                            </div><!-- services column 2 end -->
                            <!-- services column 3 start -->
                            <div class="one-third column services-column">
                                <span class="awesome"><span class="fa fa-mobile"></span></span>
                                <p class="info">
                                    Mobile First
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, <a href="index-ken-burns-dark-4.html#">consectetur adipiscing elit</a>. Sed risus purus, pellentesque a consequat a, accumsan vel libero.
                                </p>
                            </div><!-- services column 3 end -->
                            <!-- divider left bottom start -->
                            <div class="sixteen columns">
                                <div class="divider-left-bottom"></div>
                            </div><!-- divider left bottom end -->
                            <!-- divider right bottom start -->
                            <div class="sixteen columns">
                                <div class="divider-right-bottom"></div>
                            </div><!-- divider right bottom end -->
                        </div><!-- container end -->
                        <!-- closer start -->
                        <div class="fire-closer">
                            <a class="menu-state" href="index-ken-burns-dark-4.html#" id="fire-services-closer"><img alt="Closer" src="<?php echo  assets_url(); ?>images/closer.png"></a>
                        </div><!-- closer end -->
                    </div><!-- lower content end -->
                </div><!-- center container end -->
            </div><!-- center block end -->
        </div><!-- services end -->
        <!-- contact start -->
        <div class="lower-page" id="contact">
            <!-- center container start -->
            <div class="center-container">
                <!-- center block start -->
                <div class="center-block">
                    <!-- lower content start -->
                    <div class="lower-content">
                        <!-- contact intro start -->
                        <div class="contact-intro">
                            <h1>
                                CONTACT
                            </h1>
                        </div><!-- contact intro end -->
                        <!-- container start -->
                        <div class="container center">
                            <!-- divider left top start -->
                            <div class="sixteen columns">
                                <div class="divider-left-top"></div>
                            </div><!-- divider left top end -->
                            <!-- divider right top start -->
                            <div class="sixteen columns">
                                <div class="divider-right-top"></div>
                            </div><!-- divider right top end -->
                            <!-- services full start --><!-- services full end -->
                            <!-- contact column 1 start -->
                            <div class="one-third column contact-column">
                                <div class="info-address">
                                    <span class="awesome"><span class="fa fa-phone"></span></span>
                                    <p class="info">
                                        Aminurachma
                                    </p>
                                    <h3>
                                        Telepon / Whatsapp / SMS : <br> +6282299774714 
                                    </h3>
                                </div>
                            </div><!-- contact column 1 end -->
                            <!-- contact column 2 start -->
                            <div class="one-third column contact-column">
                                <div class="info-phone">
                                    <span class="awesome"><span class="fa fa-phone"></span></span>
                                    <p class="info">
                                        Ari Setya
                                    </p>
                                    <h3>
                                        Telepon / Whatsapp / SMS : <br> +6282335641082
                                    </h3>
                                </div>
                            </div><!-- contact column 2 end -->
                            <!-- contact column 3 start -->
                            <div class="one-third column contact-column">
                                <div class="info-email">
                                    <span class="awesome"><span class="fa fa-envelope"></span></span>
                                    <p class="info">
                                        E-mail
                                    </p>
                                    <p>
                                        <a href="mailto:lombamikrotik@stimata.ac.id">lombamikrotik@stimata.ac.id</a>
                                    </p>
                                </div>
                            </div><!-- contact column 3 end -->
                            <!-- divider left bottom start -->
                            <div class="sixteen columns">
                                <div class="divider-left-bottom"></div>
                            </div><!-- divider left bottom end -->
                            <!-- divider right bottom start -->
                            <div class="sixteen columns">
                                <div class="divider-right-bottom"></div>
                            </div><!-- divider right bottom end -->
                        </div><!-- container end -->
                        <!-- closer start -->
                        <div class="fire-closer">
                            <a class="menu-state" href="index-ken-burns-dark-4.html#" id="fire-contact-closer"><img alt="Closer" src="<?php echo  assets_url(); ?>images/closer.png"></a>
                        </div><!-- closer end -->
                    </div><!-- lower content end -->
                </div><!-- center container end -->
            </div><!-- center block end -->
        </div><!-- contact end -->
        <!-- scripts start -->
        <script type="text/javascript" src="<?php echo assets_url();?>jsutama/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="<?php echo assets_url();?>jsutama/velocity.min.js"></script>
        <script type="text/javascript" src="<?php echo assets_url();?>jsutama/jquery.kenburnsy.min.js"></script>
        <script type = "text/javascript" >
        //<![CDATA[
            $(function() {
                "use strict";
                $("#kenburnsy-bg").kenburnsy({
                    fullscreen: true
                });
            });
        //]]>
        </script>
        <script type="text/javascript" src="<?php echo assets_url();?>jsutama/jquery.nicescroll.3.5.4.js"></script>
        <script type="text/javascript" src="<?php echo assets_url();?>jsutama/sky.js"></script>
        <script type="text/javascript" src="<?php echo assets_url();?>jsutama/form-subscribe.js"></script>
        <script type="text/javascript" src="<?php echo assets_url();?>jsutama/form-contact.js"></script>
        <script type="text/javascript" src="<?php echo assets_url();?>knob/countdown.js"></script>
        <script type="text/javascript" src="<?php echo assets_url();?>knob/jquery.knob.js"></script>
        <script type="text/javascript" src="<?php echo assets_url();?>knob/knob.js"></script>
        <script type="text/javascript" src="<?php echo assets_url();?>jsutama/particles.js"></script>
        <script type="text/javascript" src="<?php echo assets_url();?>jsutama/app.js"></script>
        <!-- style switcher start -->
        <!-- <script type="text/javascript" src="styleswitch/styleswitch.js"></script>
		<script type="text/javascript">
        //<![CDATA[
            $(document).ready(function() {
			    "use strict";
                $(".corner").click(function() {
                $('#customizer').toggleClass('s-open');
            });
        });

        function swapStyleSheet(sheet) {
            document.getElementById('general-css').setAttribute('href', sheet);
        }
        //]]>
        </script>
        <div class="s-close" id="customizer">
            <span class="corner"></span>
            <div class="visible" id="options">
                <div class="options-head">
                    Style Switcher
                </div>
                <div class="options-segment clearfix colors-sel">
                    <div class="color-head">
                        Color Scheme
                    </div>
                    <ul class="color-scheme clearfix">
                        <li class="red">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/red" rel="red"></a>
                        </li>
                        <li class="red-2">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/red-2" rel="red-2"></a>
                        </li>
                        <li class="green">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/green" rel="green"></a>
                        </li>
                        <li class="green-2">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/green-2" rel="green-2"></a>
                        </li>
                        <li class="blue">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/blue" rel="blue"></a>
                        </li>
                        <li class="blue-2">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/blue-2" rel="blue-2"></a>
                        </li>
                        <li class="yellow">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/yellow" rel="yellow"></a>
                        </li>
                        <li class="yellow-2">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/yellow-2" rel="yellow-2"></a>
                        </li>
                        <li class="orange">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/orange" rel="orange"></a>
                        </li>
                        <li class="gold">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/gold" rel="gold"></a>
                        </li>
                        <li class="pink">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/pink" rel="pink"></a>
                        </li>
                        <li class="purple">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/purple" rel="purple"></a>
                        </li>
                        <li class="violet">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/violet" rel="violet"></a>
                        </li>
                        <li class="turquoise">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/turquoise" rel="turquoise"></a>
                        </li>
                        <li class="brick">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/brick" rel="brick"></a>
                        </li>
                        <li class="silver">
                            <a class="styleswitch" href="http://www.11-76.com/themes/sky/css/colors/silver" rel="silver"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> --><!-- style switcher end -->
        <!-- <script type="text/javascript" src="js/analytics.js"></script><!-- scripts end --> -->
    </body>
</html>