            <div class="block-header">
                <h2>
                   
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <?php if( !empty($this->session->flashdata('errors')) ):?>
                                <div class="alert bg-red alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $this->session->flashdata('errors'); ?>
                                </div>
                            <?php endif; ?>
                            <?php if( !empty($this->session->flashdata('message')) ):?>
                                <div class="alert bg-green alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                            <?php endif; ?>
                            <h2>
                                Data Tim
                            </h2><br>
                            <?php if( $cekTim === FALSE ): ?>
                                <a href="<?php echo base_url('panel/data/tim/tambah'); ?>">
                                    <button class="btn btn-large btn-primary">Tambah Tim</button>
                                </a>
                            <?php else:?>
                                <button type="button" onclick="alert('Satu sekolah hanya dapat mendaftarkan 1 Tim. \nTerima Kasih');" class="btn btn-primary waves-effect">
                                    <i class="material-icons">add</i>
                                    <span>Tambah Tim</span>
                                </button>
                            <?php endif; ?>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama Tim</th>
                                            <th>Asal Sekolah</th>
                                            <th>Pendaftar</th>
                                            <th>Verifikasi</th>
                                            <th>Tombol</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Nama Tim</th>
                                            <th>Asal Sekolah</th>
                                            <th>Pendaftar</th>
                                            <th>Verifikasi</th>
                                            <th>Tombol</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach($tim_kueri->result() as $row ):?>
                                        <tr>
                                            <td><?php echo $row->nama_tim; ?></td>
                                            <td><?php echo $row->nama_sekolah; ?></td>
                                            <td><?php echo $row->first_name; ?></td>
                                            <?php if( $row->status === '1' ):?>
                                                <td style="color: green;">Terverifikasi</td>
                                            <?php elseif( $row->status === '0'):?>
                                                <td style="color: red;">Belum di verifikasi</td>
                                            <?php else: ?>
                                                <td style="color: blue;">Null</td>
                                            <?php endif; ?>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary waves-effect dropdown-toggle" data-toggle="dropdown">
                                                        Aksi
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="<?php echo base_url('panel/data/tim/edit/'.$row->id); ?>">Cek Data</a></li>
                                                        
                                                        <li><a href="<?php echo base_url(); ?>panel/data/tim/hapus/<?php echo $row->id; ?>" onclick="return confirm('Apakah anda yakin ingin menghapus?');">Hapus Tim</a></li>
                                                        <!-- <li><a href="#">Hapus</a></li> -->
                                                    </ul>
                                                </div>

                                                <a href="<?php echo base_url('panel/data/upload/'.$row->id); ?>">
                                                    <button type="button" class="btn btn-danger waves-effect">Upload</button>
                                                </a>

                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>