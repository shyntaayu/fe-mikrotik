<!-- Advanced Form Example With Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Ubah TIM</h2>
                        </div>
                        <div class="body">
                            <?php if( $this->session->flashdata('errors') ): ?>
                                <div class="alert bg-pink alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $this->session->flashdata('errors'); ?>
                                </div>
                            <?php elseif( !empty( validation_errors() ) ): ?>
                                <div class="alert bg-pink alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo validation_errors(); ?>
                                </div>
                            <?php endif;?>
                            <?php echo  form_open_multipart('');?>
                                <?php foreach($tim->result() as $row): ?>
                                    <input type="hidden" name="timid" value="<?php echo $row->id; ?>">
                                    <fieldset>
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="header bg-red">
                                                    <h2>
                                                        Informasi Sekolah
                                                    </h2>
                                                </div>
                                                <div class="body">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" name="namaSekolah" value="<?php echo $row->nama_sekolah; ?>" >
                                                            <label class="form-label" >Nama Sekolah*</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <textarea name="alamatSekolah" cols="30" rows="5" class="form-control no-resize"  aria-required="true" aria-invalid="false"><?php echo $row->alamat_sekolah; ?></textarea>
                                                            <label class="form-label" >Alamat Sekolah*</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" name="telpSekolah" value="<?php echo $row->telepon; ?>">
                                                            <label class="form-label" >Telepon Sekolah*</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="header bg-blue-grey">
                                                    <h2>
                                                        Informasi TIM
                                                    </h2>
                                                </div>
                                                <div class="body">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" name="namaTim" value="<?php echo $row->nama_tim; ?>" class="form-control" >
                                                            <label class="form-label">Nama TIM*</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="header bg-cyan">
                                                    <h2>
                                                        Data Peserta 1 (Ketua)
                                                    </h2>
                                                </div>
                                                <div class="body">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" name="namaPesertaSatu" value="<?php echo $row->nama_peserta1; ?>" class="form-control" >
                                                            <label class="form-label">Nama Peserta*</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <textarea name="alamatPesertaSatu" cols="30" rows="3" class="form-control no-resize" ><?php echo $row->alamat_peserta1; ?></textarea>
                                                            <label class="form-label">Alamat Peserta*</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="number" name="teleponPesertaSatu" class="form-control" value="<?php echo $row->telepon_peserta1; ?>">
                                                            <label class="form-label">Nomor Telepon*</label>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="header bg-red">
                                                    <h2>
                                                        Data Peserta 2
                                                    </h2>
                                                </div>
                                                <div class="body">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" name="namaPesertaDua" class="form-control" value="<?php echo $row->nama_peserta2; ?>">
                                                            <label class="form-label">Nama Peserta*</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <textarea name="alamatPesertaDua" cols="30" rows="3" class="form-control no-resize" ><?php echo $row->alamat_peserta2; ?></textarea>
                                                            <label class="form-label">Alamat Peserta*</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="number" name="teleponPesertaDua" class="form-control" value="<?php echo $row->telpon_peserta2; ?>">
                                                            <label class="form-label">Nomor Telepon*</label>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>


                                    <fieldset>

                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="header bg-green">
                                                    <h2>
                                                        Informasi Pendamping
                                                    </h2>
                                                </div>
                                                <div class="body">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" name="namaPendamping" value="<?php echo $row->nama_pendamping; ?>" class="form-control" >
                                                            <label class="form-label">Nama Lengkap*</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <textarea name="alamatPendamping" cols="30" rows="3" class="form-control no-resize" ><?php echo $row->alamat_pendamping; ?></textarea>
                                                            <label class="form-label">Alamat*</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="email" name="emailPendamping" value="<?php echo $row->email_pendamping; ?>" class="form-control" >
                                                            <label class="form-label">Alamat Email*</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="number" name="teleponPendamping" value="<?php echo $row->telepon_pendamping; ?>" class="form-control" >
                                                            <label class="form-label">Nomor Telepon*</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </fieldset>
\
                                    <fieldset>
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="header bg-yellow">
                                                    <h2>
                                                        Upload Data
                                                    </h2>
                                                </div>
                                                <div class="body">
                                                    <div class="form-group">
                                                        <p>
                                                            <a href="<?php echo base_url('panel/data/download/').$row->data_upload;?>">Download</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>


                                    <?php if( $this->ion_auth->is_admin() ):?>
                                        <fieldset>
                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="header bg-blue">
                                                        <h2>
                                                            Menu Panitia
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="form-group">
                                                            <label>Verifikasi Tim</label>
                                                            <select class="selectpicker" name="status" required>
                                                                <option value="">.::Pilih::.</option>
                                                                <option value="1" <?php if( $row->status == '1') echo 'selected' ;?> >Ya</option>
                                                                <option value="0" <?php if( $row->status == '0') echo 'selected' ;?>>Tidak</option>
                                                            </select>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </fieldset>
                                    <?php endif;?>
                                <?php endforeach; ?>


                                <fieldset>
                                    <div class="col-lg-12 pull-right">
                                        <a href="<?php echo base_url('panel/data/tim/');?>">
                                            <button class="btn btn-lg btn-primary">Back</button>                                            
                                        </a>
                                        <button type="submit" class="btn btn-lg btn-success">Ubah Data</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Form Example With Validation