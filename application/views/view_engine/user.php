            <div class="block-header">
                <h2>
                   
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Data User
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama User</th>
                                            <th>Asal Sekolah</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Tombol</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Nama User</th>
                                            <th>Asal Sekolah</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Tombol</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach($users as $row):?>
                                            <?php $user_groups = $this->ion_auth->get_users_groups($row->id);?>
                                        <tr>
                                            <td><?php echo $row->first_name . ' ' . $row->last_name;?></td>
                                            <td><?php echo $row->company?></td>
                                            <td><?php echo $row->email; ?></td>
                                            <td>
                                                <?php foreach($user_groups->result() as $groups): ?>
                                                <p><?php echo(ucfirst($groups->name)); ?></p>
                                                <?php endforeach; ?>
                                            </td>
                                            
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        Klik Disini
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="#">Ubah Data</a></li>
                                                        <li><a href="<?php echo base_url(); ?>auth/userdel/<?php echo $row->id; ?>" onclick="return confirm('Apakah anda yakin ingin menghapus?');">Hapus</a></li>
                                                        <?php foreach($user_groups->result() as $groups): ?>
                                                            <?php if( $groups->name !== 'admin' && $groups->id !== '1' ): ?>
                                                                <li><a href="#">Jadikan Admin</a></li>
                                                            <?php endif;?>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>