<!-- Advanced Form Example With Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Pendaftaran TIM</h2>
                        </div>
                        <div class="body">
                            <?php if( $this->session->flashdata('errors') ): ?>
                                <div class="alert bg-pink alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $this->session->flashdata('errors'); ?>
                                </div>
                            <?php elseif( !empty( validation_errors() ) ): ?>
                                <div class="alert bg-pink alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo validation_errors(); ?>
                                </div>
                            <?php endif;?>
                            <?php echo  form_open_multipart('', 'id="wizard_with_validation"');?>
                                <h3>Informasi Sekolah</h3>
                                <fieldset>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="namaSekolah" value="<?php echo set_value('namaSekolah') ?>" >
                                            <label class="form-label" >Nama Sekolah*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea name="alamatSekolah" cols="30" rows="5" class="form-control no-resize"  aria-required="true" aria-invalid="false"><?php echo set_value('alamatSekolah') ?></textarea>
                                            <label class="form-label" >Alamat Sekolah*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="telpSekolah" value="<?php echo @$_POST['telpSekolah']; ?>">
                                            <label class="form-label" >Telepon Sekolah*</label>
                                        </div>
                                    </div>
                                </fieldset>

                                <h3>Informasi TIM</h3>
                                <fieldset>
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="header bg-blue-grey">
                                                <h2>
                                                    Nama TIM
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="namaTim" value="<?php echo @$_POST['namaTim']; ?>" class="form-control" >
                                                        <label class="form-label">Nama TIM*</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="header bg-cyan">
                                                <h2>
                                                    Data Peserta 1 (Ketua)
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="namaPesertaSatu" value="<?php echo @$_POST['namaPesertaSatu']; ?>" class="form-control" >
                                                        <label class="form-label">Nama Peserta*</label>
                                                    </div>
                                                </div>

                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <textarea name="alamatPesertaSatu" cols="30" rows="3" class="form-control no-resize" ><?php echo @$_POST['alamatPesertaSatu']; ?></textarea>
                                                        <label class="form-label">Alamat Peserta*</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="number" name="teleponPesertaSatu" class="form-control" value="<?php echo @$_POST['teleponPesertaSatu']; ?>">
                                                        <label class="form-label">Nomor Telepon*</label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="header bg-red">
                                                <h2>
                                                    Data Peserta 2
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="namaPesertaDua" class="form-control" value="<?php echo @$_POST['namaPesertaDua']; ?>">
                                                        <label class="form-label">Nama Peserta*</label>
                                                    </div>
                                                </div>

                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <textarea name="alamatPesertaDua" cols="30" rows="3" class="form-control no-resize" ><?php echo @$_POST['alamatPesertaDua']; ?></textarea>
                                                        <label class="form-label">Alamat Peserta*</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="number" name="teleponPesertaDua" class="form-control" value="<?php echo @$_POST['teleponPesertaDua']; ?>">
                                                        <label class="form-label">Nomor Telepon*</label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </fieldset>

                                <h3>Informasi Pendamping</h3>
                                <fieldset>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="namaPendamping" value="<?php echo @$_POST['namaPendamping']; ?>" class="form-control" >
                                            <label class="form-label">Nama Lengkap*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea name="alamatPendamping" cols="30" rows="3" class="form-control no-resize" ><?php echo @$_POST['alamatPendamping']; ?></textarea>
                                            <label class="form-label">Alamat*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="email" name="emailPendamping" value="<?php echo @$_POST['emailPendamping']; ?>" class="form-control" >
                                            <label class="form-label">Alamat Email*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" name="teleponPendamping" value="<?php echo @$_POST['teleponPendamping']; ?>" class="form-control" >
                                            <label class="form-label">Nomor Telepon*</label>
                                        </div>
                                    </div>
                                </fieldset>

                                <h3>Upload Dokumen</h3>
                                <fieldset>
                                    <div class="form-group">
                                        <label >Upload*</label>
                                        <input type="file" name="userfile" class="filestyle" data-buttonName="btn-primary" data-buttonBefore="true">
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Form Example With Validation