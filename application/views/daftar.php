﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Daftar | Lomba MIkrotik STIMATA</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo assets_url(); ?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo assets_url(); ?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo assets_url(); ?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo assets_url(); ?>css/style.css" rel="stylesheet">
</head>

<body class="signup-page">
    <div class="signup-box">
        <div class="logo">
            <a href="javascript:void(0);">Daftar Akun</b></a>
        </div>
        <div class="card">
            <div class="body">
                <?php if( $this->session->flashdata('message') ): ?>
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php endif;?>
                <?php echo form_open('', 'id="sign_up"' );?>
                    <!-- <div class="msg">Register</div> -->
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" placeholder="Nama" value="<?php echo @$_POST['name']; ?>" required autofocus>
                            <!-- @if ($errors->has('name')) -->
                                <!-- <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span> -->
                            <!-- @endif -->
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Alamat Email" value="<?php echo @$_POST['email']; ?>" required>
                            <!-- @if ($errors->has('email')) -->
                                <!-- <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span> -->
                            <!-- @endif -->
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">school</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="sekolah" placeholder="Nama Sekolah" value="<?php echo @$_POST['sekolah']; ?>" required>
                            <!-- @if ($errors->has('sekolah')) -->
                                <!-- <span class="help-block">
                                    <strong>{{ $errors->first('sekolah') }}</strong>
                                </span> -->
                            <!-- @endif -->
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="telpon" placeholder="Nomor Telepon" value="<?php echo @$_POST['telpon']; ?>" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" minlength="4" placeholder="Kata Sandi" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password_confirm" minlength="4" placeholder="Ulangi Kata Sandi" required>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">Daftar</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <a href="<?php echo base_url('login'); ?>">Sudah mendaftar?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo assets_url(); ?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo assets_url(); ?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo assets_url(); ?>plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo assets_url(); ?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo assets_url(); ?>js/admin.js"></script>
    <script src="<?php echo assets_url(); ?>js/pages/examples/sign-up.js"></script>
</body>

</html>
