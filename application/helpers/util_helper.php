<?php if(!defined('BASEPATH')) exit('Zzzzz');

if ( !function_exists('assets_url') )
{
	function assets_url()
	{
		return base_url('assets/');
	}
}

if ( !function_exists('img_url') )
{
	function img_url()
	{
		return base_url('images');
	}
}

if ( !function_exists('upload_url') )
{
	function upload_url()
	{
		return base_url('uploads');
	}
}